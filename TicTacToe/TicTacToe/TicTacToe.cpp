// TicTacToe.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <memory>
#include <string>
#include <random>
#include <optional>

#include "Grid.h"

using std::cout;
using std::cin;


bool playAgain()
{
    std::string answer;
    cout << "Play Again(Y/N)?";
    cin >> answer;
    if (answer == "y" || answer == "Y") 
    {
        return true;
    }
    return false;
}


int main()
{
    std::random_device rd;     // initialise (seed) engine
    std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister)

    cout << "Welcome to TicTacToe!\n";
    cout << "You: X, Computer: 0\n";
    bool endGame = false;
    while (!endGame) {
        std::unique_ptr<Grid> grid;
        size_t gridSize;
        cout << "Enter grid size (3-5)";
        cin >> gridSize;
        if (gridSize >= 3 && gridSize <= 5) {
            grid = std::make_unique<Grid>(gridSize);
        }
        else {
            cout << "Wrong grid size.\n";
            if (playAgain()) continue;
            else break;
        }
        cout << "Wanna play first? (Y/N)";
        std::string playFirst;
        cin >> playFirst;
        bool humanTurn;
        if (playFirst == "Y" || playFirst == "y")
        {
            humanTurn = false;
        }
        else
        {
            humanTurn = true;
        }

        while (!grid->isFull())
        {
            
            humanTurn = !humanTurn;
            if (humanTurn)
            {
                // Human Moves - draw the grid
                grid->draw();
                size_t moveIndex;
                cout << "Make your move: ";
                cin >> moveIndex;
                // Validate input
                if (cin.fail())
                {
                    cin.clear();
                    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    cout << "Invalid input. Move again. \n";
                    // Reset turn back to human
                    humanTurn = !humanTurn;
                    continue;
                }
                GridCoordinates humanMove = { moveIndex % gridSize, moveIndex / gridSize };
                // Check whether move is allowed
                if (grid->isEmptyAt(humanMove))
                {
                    grid->makeMove(Player::human,humanMove);
                    if (grid->isWinner(Player::human))
                    {
                        cout << "You WIN!\n";
                        grid->draw();
                        if (!playAgain())
                        {
                            endGame = true;
                        }
                        break;
                    }
                }
                else
                {
                    cout << "Wrong move. Move again.\n";
                    // Reset turn back to human
                    humanTurn = !humanTurn;
                    continue;
                }
            }
            else
            {
                // CPU move
                
                // Check if cpu can win in next move
                auto attackMove = grid->nextWinMove(Player::cpu);
                if (attackMove)
                {
                    grid->makeMove(Player::cpu, *attackMove);
                }
                else {
                    // Check if human has a winning move and defend
                    auto defenseMove = grid->nextWinMove(Player::human);
                    if (defenseMove)
                    {
                        grid->makeMove(Player::cpu, *defenseMove);
                    }
                    else
                    {
                        bool successfulCpuMove = false;
                        std::uniform_int_distribution<size_t> uniformDistribution(0, gridSize - 1); // guaranteed unbiased
                        // Make a random move if previous fails
                        while (!successfulCpuMove)
                        {
                            GridCoordinates cpuMove = { uniformDistribution(rng), uniformDistribution(rng) };
                            if (grid->isEmptyAt(cpuMove))
                            {
                                successfulCpuMove = true;
                                grid->makeMove(Player::cpu, cpuMove);
                            }
                        }
                    }
                }               
                
                if (grid->isWinner(Player::cpu))
                {
                    cout << "CPU WINS!\n";
                    grid->draw();
                    if (!playAgain())
                    {
                        endGame = true;
                    }
                    break;
                }
            }
        }
    }     
}
