#include <iostream>
#include "Grid.h"

using std::cout;

Grid::Grid(size_t size): gridSize(size)
{
	//initialize tiles matrix to correct size with empty values
	m_tiles.resize(gridSize, std::vector<char>(size, m_emptyChar));
}

void Grid::draw()
{
	drawDelimiter();
	for (size_t y = 0; y < gridSize; y++)
	{
		for (size_t x = 0; x < gridSize; x++)
		{
			cout << "| ";
			// output a number identifying board position when tile is empty
			if (m_tiles[x][y] == m_emptyChar)
			{
				cout << x + y * gridSize;
				if (x + y * gridSize < 10)
				{
					cout << " ";
				}
			}
			else
			{
				cout << m_tiles[x][y] << " ";
			}
		}
		cout << "|\n";
		drawDelimiter();
	}
}

bool Grid::isFull()
{
	for (const auto& it_outer:m_tiles){
		for (const auto& it_inner : it_outer)
		{
			if (it_inner == m_emptyChar) return false;
		}
	}
	return true;
}

void Grid::makeMove(bool isHuman, GridCoordinates position)
{
	if (isHuman)
	{
		m_tiles[position.x][position.y] = m_humanChar;
	}
	else
	{
		m_tiles[position.x][position.y] = m_cpuChar;
	}
}

bool Grid::isEmptyAt(GridCoordinates position)
{
	if (position.x > gridSize || position.y > gridSize)
	{
		return false;
	}
	if (m_tiles[position.x][position.y] == m_emptyChar)
	{
		return true;
	}
	return false;
}

bool Grid::isWinner(bool isHuman)
{
	if (checkXWin(isHuman) || checkYWin(isHuman) || checkDiagonalWin(isHuman))
	{
		return true;
	}
	return false;
}

std::optional<GridCoordinates> Grid::nextWinMove(bool isHuman)
{
	char moveChar = isHuman ? m_humanChar : m_cpuChar;
	for (size_t x = 0; x < gridSize; x++)
	{
		for (size_t y = 0; y < gridSize; y++)
		{
			GridCoordinates currentPos = { x,y };
			if (isEmptyAt(currentPos))
			{
				// Change current tile to simulate move
				m_tiles[currentPos.x][currentPos.y] = moveChar;
				if (isWinner(isHuman))
				{
					return currentPos;
				}
				else 
				{
					// Reset move if it was not a winning one
					m_tiles[currentPos.x][currentPos.y] = m_emptyChar;
				}
			}
			else
			{
				// Skip already full tiles
				continue;
			}
		}
	}
	return std::nullopt;
}

void Grid::drawDelimiter()
{
	cout << "+";
	for (size_t j = 0; j < gridSize; j++)
	{
		cout << "---+";
	}
	cout << "\n";
}

bool Grid::checkXWin(bool isHuman)
{
	char compareChar = isHuman ? m_humanChar : m_cpuChar;
	for (size_t x = 0; x < gridSize; x++)
	{
		bool win = true;
		for (size_t y = 0; y < gridSize; y++) 
		{
			if (m_tiles[x][y] != compareChar)
			{
				win = false;
				continue;
			}
		}
		if (win)
		{
			return true;
		}
	}
	return false;
}

bool Grid::checkYWin(bool isHuman)
{
	char compareChar = isHuman ? m_humanChar : m_cpuChar;
	for (size_t y = 0; y < gridSize; y++)
	{
		bool win = true;
		for (size_t x = 0; x < gridSize; x++)
		{
			if (m_tiles[x][y] != compareChar)
			{
				win = false;
				continue;
			}
		}
		if (win)
		{
			return true;
		}
	}
	return false;
}

bool Grid::checkDiagonalWin(bool isHuman)
{
	char compareChar = isHuman ? m_humanChar : m_cpuChar;
	bool win1 = true, win2 = true;
	for (size_t i = 0; i < gridSize; i++)
	{
		if (m_tiles[i][i] != compareChar)
		{
			win1 = false;
			continue;
		}
	}
	for (size_t j = 0; j < gridSize; j++)
	{
		if (m_tiles[j][gridSize - 1LL - j] != compareChar)
		{
			win2 = false;
			continue;
		}
	}
	return win1 || win2;
}
