#pragma once
#include <vector>
#include <optional>

// Helper struct for grid coordinates
struct GridCoordinates {
	size_t x;
	size_t y;
};

// Constants for identifying cpu/human when passing parameters
struct Player 
{
	static constexpr bool human = true;
	static constexpr bool cpu = false;
};

/// <summary>
/// Grid class for TicTacToe game. The grid is square, size is passed as parameter in the constructor.
/// </summary>

class Grid
{
public:
	Grid(size_t size);
	// Draw the grid in its current state to the console.
	void draw();
	bool isFull();
	void makeMove(bool isHuman, GridCoordinates position);
	bool isEmptyAt(GridCoordinates position);
	// Check if a player won the board (bool parameter switches human/cpu check).
	bool isWinner(bool isHuman);
	// Find the next winning move for cpu/human (if exists)
	std::optional<GridCoordinates> nextWinMove(bool isHuman);
private:
	// Draws horizontal lines between grid tiles
	void drawDelimiter();
	std::vector<std::vector<char>> m_tiles;
	size_t gridSize;
	// Characters used for identifying empty/full tiles
	static constexpr char m_humanChar = 'X';
	static constexpr char m_cpuChar = 'O';
	static constexpr char m_emptyChar = '_';
	// Check win on X axis (rows)
	bool checkXWin(bool isHuman);
	// Check win on Y axis (columns)
	bool checkYWin(bool isHuman);
	// Check win on diagonals
	bool checkDiagonalWin(bool isHuman);
};

